#include <iostream>
#include "animal.h"
#include "bird.h"
#include "fish.h"

int main() {

    animal puffy; // puffy is an instance of the animal class

    puffy.eat();
    puffy.sleep();
    bird birdy;
    birdy.eat();
    birdy.fly();
    fish f;
    f.eat();
    f.swim();

    return 0;
}

