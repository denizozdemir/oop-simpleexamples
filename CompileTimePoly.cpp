
#include<iostream>
using namespace std;

class Base {
protected:
public:
    void display() {cout << "Parent Class" <<endl;}
};
class Derived1: public Base {
public:
    void display () {cout<< "Derived Class 1"<<endl;}
};
int main() {
    Base* base;
    Derived1 derived1;
    base = &derived1;
    base->display();
    return 0;
}




