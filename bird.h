
#ifndef TEKNIK_SUNUM_BIRD_H
#define TEKNIK_SUNUM_BIRD_H


#include "animal.h"

//Derived class
class bird : public animal{
public:
    void fly();
    void sing();
private:

};


#endif //TEKNIK_SUNUM_BIRD_H
