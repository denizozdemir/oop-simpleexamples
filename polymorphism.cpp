#include <iostream>
using namespace std;

class printData {
public:
    void print(int i) {cout << "Printing int: " << i << endl;}
    void print(double  f) {cout << "Printing double: " << f << endl;}
};
int main() {
    printData pd;
    // Call print to print integer
    pd.print(5);
    // Call print to print double
    pd.print(500.263);
    return 0;
}


