
#include<iostream>
#include<typeinfo>
using namespace std;
class Base {
protected:
public:
    virtual void display() {cout << "Parent Class" <<endl;}
};
class Derived: public Base {
public:
    void display () {cout<< "Derived Class"<<endl;}
};
int main() {
    Base* base;
    Derived derived;
    base = &derived;
    base->display();
    return 0;
}




